aptproxy
===

aptproxy is a simple minimal HTTP cache for APT package repositories.
It has almost no features:

* supports the standard HTTP proxy protocol
* supports the CONNECT protocol to avoid breaking misconfigured
  clients that might try to connect to https sites through it, though
  it won't cache anything in that case
* it can send conditional GET requests for Release files and such
* it won't do conditional requests for .deb packages because it
  assumes those to be immutable
* it is not a real HTTP caching proxy because it does not understand
  the Vary header
* it will prevent connections to localhost or to private network
  ranges, including domains that resolve to those addresses
* multiple concurrent requests for the same URL will result in a
  single upstream request

## Installation

All installation methods involve building aptproxy from source, so
you're going to need a Go environment installed on your computer. On a
Debian system:

```shell
sudo apt install golang
```

### From the source repo

To build the package you can clone this repository and run

```shell
go build .
```

Then install the resulting *aptproxy* binary wherever you want.

### From source via *go install*

You don't even need to clone this repository by installing the tool
with:

```shell
go install git.autistici.org/pipelines/tools/aptproxy@latest
```

This will result in an *aptproxy* binary likely appearing in
*~/go/bin*.

### Debian package

The above methods aren't really enough for a system daemon, which is
why we recommend installing aptproxy as a Debian package. You can
build the package, after having cloned this repository, with the
standard Debian developer tools:

```shell
sudo apt install dpkg-dev debhelper dh-golang fakeroot
dpkg-buildpackage -rfakeroot -us -uc -b
sudo dpkg -i ../aptproxy*.deb
```
