package main

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"flag"
	"io"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"sync"
	"time"

	"golang.org/x/sync/singleflight"
)

var (
	addr     = flag.String("addr", ":3142", "address to listen on")
	cacheDir = flag.String("dir", "", "cache directory")
)

// Return true if we think the file is immutable, regardless of the
// HTTP headers we got in the response.
func storeForever(resp *http.Response) bool {
	return resp.Header.Get("Content-Type") == "application/vnd.debian.binary-package"
}

// Metadata associated with a URL.
type urlMeta struct {
	Etag         string    `json:"etag"`
	LastModified string    `json:"lastModified"`
	Expires      time.Time `json:"expires"`
	Permanent    bool      `json:"permanent"`
	ContentType  string    `json:"contentType"`
}

// Update the urlMeta object using data from the HTTP response.
func (m *urlMeta) updateFromResponse(resp *http.Response) *urlMeta {
	if m == nil {
		m = new(urlMeta)
	}

	m.Etag = resp.Header.Get("ETag")
	m.LastModified = resp.Header.Get("Last-Modified")
	m.ContentType = resp.Header.Get("Content-Type")
	m.Permanent = storeForever(resp)

	switch s := resp.Header.Get("Expires"); s {
	case "-1":
		m.Permanent = true
	default:
		m.Expires, _ = time.Parse(s, http.TimeFormat)
	}

	return m
}

var dialer = new(net.Dialer)

// Forbid connections to loopback and private network space.
func isAllowed(ip net.IP) bool {
	return !(ip.IsLoopback() || ip.IsPrivate() || ip.IsLinkLocalUnicast() || !ip.IsGlobalUnicast())
}

// Provide our own DialContext function that can check the resolved IP
// addresses for unwanted ranges.
func safeDialContext(ctx context.Context, network, addr string) (net.Conn, error) {
	// Resolve the target address. We check all IPs returned by
	// the DNS lookup, but then unfortunately we pass the original
	// host back to DialContext(), otherwise we'll have to code
	// the retry behavior ourselves...
	host, _, _ := net.SplitHostPort(addr)
	ips, err := net.LookupIP(host)
	if err != nil {
		return nil, err
	}
	for _, ip := range ips {
		if !isAllowed(ip) {
			return nil, errors.New("forbidden")
		}
	}

	return dialer.DialContext(ctx, network, addr)
}

// Hop-by-hop headers that should be dropped in outbound requests.
var hopHeaders = []string{
	"Alt-Svc",
	"Connection",
	"Proxy-Connection", // non-standard but still sent by libcurl and rejected by e.g. google
	"Keep-Alive",
	"Proxy-Authenticate",
	"Proxy-Authorization",
	"Te",      // canonicalized version of "TE"
	"Trailer", // not Trailers per URL above; https://www.rfc-editor.org/errata_search.php?eid=4522
	"Transfer-Encoding",
	"Upgrade",
}

// Custom http.Client that uses safeDialContext.
var client = &http.Client{
	Timeout: 10 * time.Second,
	CheckRedirect: func(req *http.Request, via []*http.Request) error {
		return http.ErrUseLastResponse
	},
	Transport: &http.Transport{
		Proxy:                 http.ProxyFromEnvironment,
		DialContext:           safeDialContext,
		ForceAttemptHTTP2:     true,
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
	},
}

// Makes a (potentially conditional) GET request for a remote URL.
// Conditional parameters are taken from 'meta' if not nil. An updated
// urlMeta object is returned when the response has status 200.
func download(origReq *http.Request, meta *urlMeta) (*http.Response, *urlMeta, error) {
	// Make a copy of the request.
	req, err := http.NewRequest(origReq.Method, origReq.URL.String(), nil)
	if err != nil {
		return nil, nil, err
	}

	req.Header = req.Header.Clone()
	for _, k := range hopHeaders {
		req.Header.Del(k)
	}
	if meta != nil {
		if meta.Etag != "" {
			req.Header.Set("If-None-Match", meta.Etag)
		} else if meta.LastModified != "" {
			req.Header.Set("If-Modified-Since", meta.LastModified)
		}
	}

	resp, err := client.Do(req.WithContext(origReq.Context()))
	if err != nil {
		return nil, nil, err
	}

	// Update metadata only on status 200.
	if resp.StatusCode == http.StatusOK {
		meta = meta.updateFromResponse(resp)
	}

	return resp, meta, nil
}

func copyHeaders(w http.ResponseWriter, hdr http.Header) {
	for k, values := range hdr {
		for _, v := range values {
			w.Header().Add(k, v)
		}
	}
}

// Generic response, which must be disconnected from the upstream
// http.Response because due to the singleflight.Group we might return
// the same result to multiple clients.
type response interface {
	serve(http.ResponseWriter)
	statusCode() int
	cached() bool
}

// Response from upstream (for errors, redirects, etc).
type upstreamResponse struct {
	status int
	hdr    http.Header
	body   []byte
}

func responseFromUpstream(resp *http.Response) (*upstreamResponse, error) {
	r := &upstreamResponse{
		status: resp.StatusCode,
		hdr:    resp.Header.Clone(),
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	r.body = body

	return r, nil
}

func (r *upstreamResponse) serve(w http.ResponseWriter) {
	copyHeaders(w, r.hdr)
	w.WriteHeader(r.status)
	w.Write(r.body) //nolint:errcheck
}

func (r *upstreamResponse) statusCode() int { return r.status }
func (r *upstreamResponse) cached() bool    { return false }

// Response from the local filesystem-backed cache.
type cachedResponse struct {
	hdr          http.Header
	localPath    string
	lastModified time.Time
	request      *http.Request
	fromCache    bool
}

func responseFromCache(req *http.Request, path string, m *urlMeta, fromCache bool) *cachedResponse {
	r := &cachedResponse{
		request:   req,
		localPath: path,
		fromCache: fromCache,
		hdr:       make(http.Header),
	}

	if m.Etag != "" {
		r.hdr.Set("ETag", m.Etag)
	}

	if m.ContentType != "" {
		r.hdr.Set("Content-Type", m.ContentType)
	}

	if m.LastModified != "" {
		r.lastModified, _ = time.Parse(m.LastModified, http.TimeFormat)
	}

	return r
}

func (r *cachedResponse) serve(w http.ResponseWriter) {
	copyHeaders(w, r.hdr)

	f, err := os.Open(r.localPath)
	if err != nil {
		log.Printf("error opening file: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer f.Close()

	http.ServeContent(w, r.request, "", r.lastModified, f)
}

func (r *cachedResponse) statusCode() int { return http.StatusOK }
func (r *cachedResponse) cached() bool    { return r.fromCache }

// URL cache backed by the local filesystem.
type cache struct {
	dir string

	dl singleflight.Group
}

func (c *cache) pathFor(uri *url.URL) string {
	h := sha256.New()
	io.WriteString(h, uri.String()) //nolint:errcheck
	s := hex.EncodeToString(h.Sum(nil))

	// Two-level directory hierarchy.
	return filepath.Join(c.dir, s[0:1], s[1:2], s)
}

// Download a file to the cache.
func (c *cache) writeFile(resp *http.Response) error {
	dest := c.pathFor(resp.Request.URL)
	if err := os.MkdirAll(filepath.Dir(dest), 0700); err != nil {
		return err
	}

	tmpf, err := os.CreateTemp(c.dir, ".download-*")
	if err != nil {
		return err
	}
	defer func() {
		tmpf.Close()
		os.Remove(tmpf.Name())
	}()

	if _, err := io.Copy(tmpf, resp.Body); err != nil {
		return err
	}

	return os.Rename(tmpf.Name(), dest)
}

// Update the URL metadata in the cache.
func (c *cache) writeMeta(uri *url.URL, m *urlMeta) error {
	f, err := os.Create(c.pathFor(uri) + ".meta")
	if err != nil {
		return err
	}
	defer f.Close()
	return json.NewEncoder(f).Encode(m)
}

// Read URL metadata from the cache.
func (c *cache) readMeta(uri *url.URL) (*urlMeta, error) {
	// Also check for existence of the actual file, to avoid state
	// desynchronization.
	path := c.pathFor(uri)
	if _, err := os.Stat(path); err != nil {
		return nil, err
	}

	f, err := os.Open(path + ".meta")
	if err != nil {
		return nil, err
	}
	defer f.Close()

	var m urlMeta
	return &m, json.NewDecoder(f).Decode(&m)
}

// Download a file into the cache and store it alongside the
// associated URL metadata.
func (c *cache) ingest(resp *http.Response, m *urlMeta) error {
	if err := c.writeFile(resp); err != nil {
		return err
	}
	return c.writeMeta(resp.Request.URL, m)
}

// Actually perform the cache lookup: if the URL is not cached,
// download it; if it's cached, attempt a conditional download to see
// if it needs a refresh.
func (c *cache) doLookup(req *http.Request) (response, error) {
	meta, _ := c.readMeta(req.URL)

	// If the cached entry hasn't expired yet, do not even attempt to download.
	if meta != nil && (meta.Permanent || meta.Expires.After(time.Now())) {
		return responseFromCache(req, c.pathFor(req.URL), meta, true), nil
	}

	resp, meta, err := download(req, meta)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	switch resp.StatusCode {
	case http.StatusOK:
		if err := c.ingest(resp, meta); err != nil {
			return nil, err
		}
		return responseFromCache(req, c.pathFor(req.URL), meta, false), nil

	case http.StatusNotModified:
		return responseFromCache(req, c.pathFor(req.URL), meta, true), nil

	default:
		return responseFromUpstream(resp)
	}
}

// Lookup a URL from the cache. If multiple clients request the same
// file at the same time, we will still only perform a single upstream
// connection.
func (c *cache) lookup(req *http.Request) (response, error) {
	resp, err, _ := c.dl.Do(req.URL.String(), func() (any, error) {
		return c.doLookup(req)
	})
	if err != nil {
		return nil, err
	}
	return resp.(response), nil
}

// Cache HTTP handler, for the traditional proxy protocol (GET <URL>).
func (c *cache) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet && req.Method != http.MethodHead {
		http.Error(w, "method not allowed", http.StatusMethodNotAllowed)
		return
	}

	if req.URL.Scheme == "" {
		http.NotFound(w, req)
		return
	}

	resp, err := c.lookup(req)
	if err != nil {
		log.Printf("lookup error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	log.Printf("%s %s %s -> %d %v", remoteIP(req), req.Method, req.URL.String(), resp.statusCode(), resp.cached())

	resp.serve(w)
}

// CONNECT HTTP handler, for the proxy tunneling protocol.
func handleConnect(w http.ResponseWriter, req *http.Request) {
	log.Printf("%s CONNECT %s", remoteIP(req), req.URL.Host)

	remote, err := safeDialContext(req.Context(), "tcp", req.URL.Host)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer remote.Close()

	conn, _, err := w.(http.Hijacker).Hijack()
	if err != nil {
		log.Printf("CONNECT: couldn't hijack connection: %v", err)
		return
	}
	defer conn.Close()

	io.WriteString(conn, "HTTP/1.0 200 OK Connection Established\r\n\r\n") //nolint:errcheck

	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		io.Copy(conn, remote) //nolint:errcheck
		defer wg.Done()
	}()
	go func() {
		io.Copy(remote, conn) //nolint:errcheck
		defer wg.Done()
	}()

	wg.Wait()
}

func remoteIP(req *http.Request) string {
	host, _, _ := net.SplitHostPort(req.RemoteAddr)
	return host
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	if *cacheDir == "" {
		log.Fatal("Must specify --dir")
	}

	c := &cache{dir: *cacheDir}
	h := http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		if req.Method == http.MethodConnect {
			handleConnect(w, req)
		} else {
			c.ServeHTTP(w, req)
		}
	})

	log.Fatal(http.ListenAndServe(*addr, h))
}
